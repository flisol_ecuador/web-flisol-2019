# Página web para FLISoL (Festival Latinoamericano de Intalacion de Software Libre) edicion nº 15

> El FLISOL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y personas en general que se encuentren ávidas de conocimiento y ganas de aprender sobre la cultura libre. 


* Lugar: Facultad de Especialidades Empresariales, Universidad Católica Santiago de Guayaquil (Piso 7 y 8).
* Dirección: Calle Terán entre Simón Bolivar y Sucre.
* Fecha: Sábado 18 de Mayo 2019
* Horario: 08h30 - 17h00 

# Descripcion del sitio

Es una página para Flisol Ecuador, con el objetivo de visibilizar todos los eventos FLISoL en el Ecuador, en el año 2019 solo esta incluida la sede Guayaquil.

## Redes Sociales:

* Instagram: @flisol.guayaquil

* Facebook: flisol.guayaquil

* Twitter: @flisol.guayaquil

### Hashtags:

#FLISoL #Ecuador #Guayaquil

#FLISoL2019 #FLISoLGYE 

## Información de contacto:

* Correo: guayaquil@flisol.ec

* Grupo de Organización en Telegram

* Canal de noticias Telegram

> Este sitio web fue creado por el apoyo de activistas:
> 

* Jorge Ortega

* Eduardo Israel

* Jhon Merchan 

* Francisco Silva
